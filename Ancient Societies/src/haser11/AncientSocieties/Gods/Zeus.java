package haser11.AncientSocieties.Gods;

import haser11.AncientSocieties.Config.Config;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.LightningStrike;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

public class Zeus implements Listener 
{
	Plugin plugin;
	Config cm = new Config(plugin);
	
	public Zeus(Plugin p)
	{
		this.plugin = p;
	}
	
	@EventHandler
	public void superLightning(PlayerInteractEvent event)
	{
		Player player = event.getPlayer();
		if(player.getItemInHand().getType().equals(Material.DIAMOND_AXE) && cm.getEntryFromFile(Config.UUIDRegistry, "Gods.Zeus") == player.getName())
		{
			Block b = player.getTargetBlock(null, 100);
			Location loc = b.getLocation();
			
			Location loc1 = new Location(loc.getWorld(), loc.getX() + 1, loc.getY(), loc.getZ());
			Location loc2 = new Location(loc.getWorld(), loc.getX() - 1, loc.getY(), loc.getZ());
			Location loc3 = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ() + 1);
			Location loc4 = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ() - 1);
			Location loc5 = new Location(loc.getWorld(), loc.getX() + 1, loc.getY(), loc.getZ() + 1);
			Location loc6 = new Location(loc.getWorld(), loc.getX() + 1, loc.getY(), loc.getZ() - 1);
			Location loc7 = new Location(loc.getWorld(), loc.getX() - 1, loc.getY(), loc.getZ() + 1);
			Location loc8 = new Location(loc.getWorld(), loc.getX() - 1, loc.getY(), loc.getZ() - 1);
			
			Bukkit.getWorld(loc.getWorld().getName()).spawn(loc, LightningStrike.class);
			Bukkit.getWorld(loc.getWorld().getName()).spawn(loc1, LightningStrike.class);
			Bukkit.getWorld(loc.getWorld().getName()).spawn(loc2, LightningStrike.class);
			Bukkit.getWorld(loc.getWorld().getName()).spawn(loc3, LightningStrike.class);
			Bukkit.getWorld(loc.getWorld().getName()).spawn(loc4, LightningStrike.class);
			Bukkit.getWorld(loc.getWorld().getName()).spawn(loc5, LightningStrike.class);
			Bukkit.getWorld(loc.getWorld().getName()).spawn(loc6, LightningStrike.class);
			Bukkit.getWorld(loc.getWorld().getName()).spawn(loc7, LightningStrike.class);
			Bukkit.getWorld(loc.getWorld().getName()).spawn(loc8, LightningStrike.class);
		}
	}
}
