package haser11.AncientSocieties.Gods;

import haser11.AncientSocieties.Config.Config;

import java.io.File;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class God 
{
	Plugin plugin;
	
	public God(Plugin p)
	{
		this.plugin = p;
	}
	
	public void createGodFileStructure()
	{
		File file = new File(Config.mainDir + File.separator + "Gods");
		Config config = new Config(plugin);
		if(!file.exists())
		{
			config.createFile(file);
			
			config.addEntryToFile(file, "Gods.Zeus", null);
			config.addEntryToFile(file, "Gods.Hera", null);
			config.addEntryToFile(file, "Gods.Poseidon", null);
			config.addEntryToFile(file, "Gods.Dementer", null);
			config.addEntryToFile(file, "Gods.Ares", null);
			config.addEntryToFile(file, "Gods.Athena", null);
			config.addEntryToFile(file, "Gods.Apollo", null);
			config.addEntryToFile(file, "Gods.Artemis", null);
			config.addEntryToFile(file, "Gods.Hephaestus", null);
			config.addEntryToFile(file, "Gods.Aphrodite", null);
			config.addEntryToFile(file, "Gods.Hermes", null);
			config.addEntryToFile(file, "Gods.Dionysus", null);
			config.addEntryToFile(file, "Gods.Hades", null);
			config.addEntryToFile(file, "Gods.Hypnos", null);
			config.addEntryToFile(file, "Gods.Nike", null);
			config.addEntryToFile(file, "Gods.Janus", null);
			config.addEntryToFile(file, "Gods.Nemesis", null);
			config.addEntryToFile(file, "Gods.Iris", null);
			config.addEntryToFile(file, "Gods.Hecate", null);
			config.addEntryToFile(file, "Gods.Tyche", null);
		}
	}
	
	public void setGod(Player p, String godName)
	{
		Config cm = new Config(plugin);
		File file = new File(cm.mainDir + File.separator + "Gods.yml");
		
		cm.addEntryToFile(file, "Gods." + godName, p.getName());
	}
}
