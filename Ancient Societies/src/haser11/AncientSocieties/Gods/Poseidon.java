package haser11.AncientSocieties.Gods;

import java.io.File;

import haser11.AncientSocieties.Config.Config;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

public class Poseidon implements Listener
{
	Plugin plugin;
	Config cm = new Config(plugin);
	
	public Poseidon(Plugin p)
	{
		this.plugin = p;
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event)
	{
		File file = new File(cm.mainDir + File.separator + "Gods.yml");
		if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && cm.getEntryFromFile(file, "Gods.Poseidon").equals(event.getPlayer().getName()))
		{
			Player player = event.getPlayer();
			Block b = player.getTargetBlock(null, 100);
			
			if(b.getType().equals(Material.WATER))
			{
				Location loc = b.getLocation();
				
				Location loc1 = new Location(loc.getWorld(), loc.getX(), loc.getY() + 5, loc.getZ() + 1);
				Location loc2 = new Location(loc.getWorld(), loc.getX(), loc.getY() + 5, loc.getZ() + 2);
				Location loc3 = new Location(loc.getWorld(), loc.getX(), loc.getY() + 5, loc.getZ() - 1);
				Location loc4 = new Location(loc.getWorld(), loc.getX(), loc.getY() + 5, loc.getZ() - 2);
				Location loc5 = new Location(loc.getWorld(), loc.getX(), loc.getY() + 5, loc.getZ());
				
				Location loc6 = new Location(loc.getWorld(), loc.getX() + 1, loc.getY() + 4, loc.getZ() + 1);
				Location loc7 = new Location(loc.getWorld(), loc.getX() + 1, loc.getY() + 4, loc.getZ() + 2);
				Location loc8 = new Location(loc.getWorld(), loc.getX() + 1, loc.getY() + 4, loc.getZ() - 1);
				Location loc9 = new Location(loc.getWorld(), loc.getX() + 1, loc.getY() + 4, loc.getZ() - 2);
				Location loc10 = new Location(loc.getWorld(), loc.getX() + 1, loc.getY() + 4, loc.getZ());
				
				Location loc11 = new Location(loc.getWorld(), loc.getX() + 2, loc.getY() + 3, loc.getZ() + 1);
				Location loc12 = new Location(loc.getWorld(), loc.getX() + 2, loc.getY() + 3, loc.getZ() + 2);
				Location loc13 = new Location(loc.getWorld(), loc.getX() + 2, loc.getY() + 3, loc.getZ() - 1);
				Location loc14 = new Location(loc.getWorld(), loc.getX() + 2, loc.getY() + 3, loc.getZ() - 2);
				Location loc15 = new Location(loc.getWorld(), loc.getX() + 2, loc.getY() + 3, loc.getZ());
				
				Location loc16 = new Location(loc.getWorld(), loc.getX() + 3, loc.getY() + 2, loc.getZ() + 1);
				Location loc17 = new Location(loc.getWorld(), loc.getX() + 3, loc.getY() + 2, loc.getZ() + 2);
				Location loc18 = new Location(loc.getWorld(), loc.getX() + 3, loc.getY() + 2, loc.getZ() - 1);
				Location loc19 = new Location(loc.getWorld(), loc.getX() + 3, loc.getY() + 2, loc.getZ() - 2);
				Location loc20 = new Location(loc.getWorld(), loc.getX() + 3, loc.getY() + 2, loc.getZ());
				
				loc1.getBlock().setType(Material.WATER);
				loc2.getBlock().setType(Material.WATER);
				loc3.getBlock().setType(Material.WATER);
				loc4.getBlock().setType(Material.WATER);
				loc5.getBlock().setType(Material.WATER);
				
				loc6.getBlock().setType(Material.WATER);
				loc7.getBlock().setType(Material.WATER);
				loc8.getBlock().setType(Material.WATER);
				loc9.getBlock().setType(Material.WATER);
				loc10.getBlock().setType(Material.WATER);
				
				loc11.getBlock().setType(Material.WATER);
				loc12.getBlock().setType(Material.WATER);
				loc13.getBlock().setType(Material.WATER);
				loc14.getBlock().setType(Material.WATER);
				loc15.getBlock().setType(Material.WATER);
				
				loc16.getBlock().setType(Material.WATER);
				loc17.getBlock().setType(Material.WATER);
				loc18.getBlock().setType(Material.WATER);
				loc19.getBlock().setType(Material.WATER);
				loc20.getBlock().setType(Material.WATER);
			}
		}
	}
}
