package haser11.AncientSocieties.Main;

import haser11.AncientSocieties.Commands.BasicCommands;
import haser11.AncientSocieties.Config.Config;
import haser11.AncientSocieties.Config.LoginManager;
import haser11.AncientSocieties.Gods.God;
import haser11.AncientSocieties.Gods.Poseidon;
import haser11.AncientSocieties.Gods.Zeus;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * Ancient Societies is a private plugin created for the Ancient Societies server
 * It is developed and maintained by haser11
 * 
 * @author haser11
 *
 */

public class AncientSocieties extends JavaPlugin
{

	//Class instances
	Config cm = new Config(this);
	
	
	@Override
	public void onEnable()
	{
		Bukkit.getLogger().log(Level.INFO, "Ancient Societies is starting up!");
		PluginManager manager = Bukkit.getServer().getPluginManager();
		
		//First time checks
		Config.checkIfNewServer();
		Config.createUUIDRegistry();
		God g = new God(this);
		g.createGodFileStructure();
	    
	
		//Class registry
		new God(this);
		new Config(this);
		new Zeus(this);
		new Poseidon(this);
		new LoginManager(this);
		
		//Listener registry
		manager.registerEvents(new Zeus(this), this);
		manager.registerEvents(new Poseidon(this), this);
		manager.registerEvents(new LoginManager(this), this);
		
		//Command registry
		getCommand("setGod").setExecutor(new BasicCommands(this));
	}
	
	
	@Override
	public void onDisable()
	{
		Bukkit.getLogger().log(Level.INFO, "Ancient Socities is shutting down!");
	}
	
}
