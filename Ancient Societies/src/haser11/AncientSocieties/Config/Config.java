package haser11.AncientSocieties.Config;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;


public class Config
{
	public static File mainDir = new File("plugins" + File.separator + "Ancient Societies");
	public static File usersDir = new File(mainDir + File.separator + "users");
	public static File UUIDRegistry = new File(mainDir + File.separator + "UUIDS.yml");
	static FileConfiguration config = null;
	Plugin plugin;
	
	public Config(Plugin p)
	{
		this.plugin = p;
	}
	
	public static boolean checkIfNewServer()
	{
		if(!mainDir.exists() && !usersDir.exists())
		{
			Bukkit.getLogger().log(Level.WARNING, "Ancient Societies config doesn't exist, trying to create it...");
			try
			{
				usersDir.mkdir();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return true;
		}
		else
		{
			return false;
		}
			
	}
	
	public static void createUUIDRegistry()
	{
		if(!UUIDRegistry.exists())
		{
			try
			{
				UUIDRegistry.createNewFile();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
	}
	
	public void createFile(File file)
	{
		File fileToMake = new File(file + ".yml");
		if(!fileToMake.exists())
		{
			try
			{
				fileToMake.createNewFile();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void renameFile(File directory, File fileToRename, String newName)
	{
		if(fileToRename.exists())
		{
			try
			{
				fileToRename.renameTo(new File(directory + newName + ".yml"));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void addEntryToFile(File file, String path, Object obj)
	{
		this.config = YamlConfiguration.loadConfiguration(file);
		this.config.set(path, obj);
		try
		{
			this.config.save(file);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public Object getEntryFromFile(File file, String path)
	{
		this.config = YamlConfiguration.loadConfiguration(file);
		return this.config.get(path);
	}
	
	public void saveConfig()
	{
		
	}
	
}