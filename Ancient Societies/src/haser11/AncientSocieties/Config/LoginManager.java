package haser11.AncientSocieties.Config;

import java.io.File;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.Plugin;

public class LoginManager implements Listener
{
	Plugin plugin;
	
	public LoginManager(Plugin p)
	{
		this.plugin = p;
	}
	
	@EventHandler
	public void onLogin(PlayerLoginEvent event)
	{
		Config cm = new Config(plugin);
		if(cm.getEntryFromFile(cm.UUIDRegistry, "Players." + event.getPlayer().getUniqueId()) == null)
		{
			cm.addEntryToFile(cm.UUIDRegistry, "Players." + event.getPlayer().getUniqueId(), event.getPlayer().getName());
			cm.createFile(new File(cm.usersDir + event.getPlayer().getName()));
		}
		else if(cm.getEntryFromFile(cm.UUIDRegistry, "Players." + event.getPlayer().getUniqueId()) != event.getPlayer().getName())
		{
			cm.renameFile(cm.usersDir, new File(cm.usersDir + File.separator + cm.getEntryFromFile(cm.UUIDRegistry, "Players." + 
		    event.getPlayer().getUniqueId())), event.getPlayer().getName());
			cm.addEntryToFile(cm.UUIDRegistry, "Players." + event.getPlayer().getUniqueId(), event.getPlayer().getName());
		}
	}
}
