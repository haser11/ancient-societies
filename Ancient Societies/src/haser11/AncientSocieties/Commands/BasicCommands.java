package haser11.AncientSocieties.Commands;

import haser11.AncientSocieties.Gods.God;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class BasicCommands implements CommandExecutor
{
	Plugin plugin;
	
	public BasicCommands(Plugin p)
	{
		this.plugin = p;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label,
			String[] args) 
	{
		if(command.getName().equalsIgnoreCase("setGod"))
		{
			if(args.length == 2)
			{
				God g = new God(plugin);
				Player p = Bukkit.getServer().getPlayer(args[0]);
				
				g.setGod(p, args[1]);
			}
		}
		
		return false;
	}

}
